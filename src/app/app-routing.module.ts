import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HomeComponent } from './home/home.component';
import { AuthGuardService } from './services/auth-guard.service';
import { SearchDistributorComponent } from './search-distributor/search-distributor.component';
import { SearchRetailerComponent } from './search-retailer/search-retailer.component';
import { ProfileComponent } from './profile/profile.component';
import { ViewCustomerComponent } from './view-customer/view-customer.component';
import { SignupComponent } from './signup/signup.component';
import { ViewProfileComponent } from './view-profile/view-profile.component';
import { ViewSearchRetailerProfileComponent } from './view-search-retailer-profile/view-search-retailer-profile.component';
import { ViewSearchDistributorProfileComponent } from './view-search-distributor-profile/view-search-distributor-profile.component';


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'retailer', component: HomeComponent },
  { path: 'distributor', component: HomeComponent },
  { path: 'signup', component: SignupComponent },
  {
    path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuardService], children: [
      { path: 'searchDistributor', component: SearchDistributorComponent, canActivate: [AuthGuardService] },
      { path: 'searchRetailer', component: SearchRetailerComponent, canActivate: [AuthGuardService] },
      { path: 'myProfile', component: ProfileComponent, canActivate: [AuthGuardService] },
      { path: 'viewMyCustomers', component: ViewCustomerComponent, canActivate: [AuthGuardService] },
    ]
  },
  { path: 'viewProfile/:id/:id1/:id2', component: ViewProfileComponent, canActivate: [AuthGuardService] },
  { path: 'viewSearchRetailerProfile/:id', component: ViewSearchRetailerProfileComponent, canActivate: [AuthGuardService] },
  { path: 'viewSearchDistributorProfile/:id', component: ViewSearchDistributorProfileComponent, canActivate: [AuthGuardService] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {


}
