import { Component, OnInit } from '@angular/core';
import { Retailer } from '../models/retailer';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-view-search-retailer-profile',
  templateUrl: './view-search-retailer-profile.component.html',
  styleUrls: ['./view-search-retailer-profile.component.css']
})
export class ViewSearchRetailerProfileComponent implements OnInit {

  shopkeeper_id: string;
  auth: any = JSON.parse(localStorage.getItem('auth'));
  retailer: Retailer;

  constructor(private route: ActivatedRoute, private http: HttpClient, private router:Router) { }

  ngOnInit() {
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', 'Token '+ this.auth.token);
    this.shopkeeper_id = this.route.snapshot.paramMap.get("id");
    this.http.get("http://localhost:8000/vendor-service/v1/shopkeepers/" + this.shopkeeper_id,{headers: headers}).subscribe((data: Retailer) => {
      this.retailer = data
    }), error => {
      alert("Login Error");
      console.error(error);
    };
  }

}
