import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  auth: any = JSON.parse(localStorage.getItem('auth'));
  loginType: string = this.auth.loginType;
  distributor:string = "distributor";
  retailer:string = "retailer";

  constructor(private router:Router,private authService:AuthService) { }

  ngOnInit() {
  }

  logout(){
    localStorage.removeItem("auth");
    this.router.navigate(["home"]);

  }

}
