import { Component, OnInit } from '@angular/core';
import { ProfileComponent } from '../profile/profile.component';
import { Distributor } from '../models/distributor';
import { Retailer } from '../models/retailer';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-view-customer',
  templateUrl: './view-customer.component.html',
  styleUrls: ['./view-customer.component.css']
})
export class ViewCustomerComponent implements OnInit {
  auth: any = JSON.parse(localStorage.getItem('auth'));
  loginType: string = this.auth.loginType;
  distributor:Distributor;
  retailers:Retailer[];
  id: string = this.auth.id;

  constructor(private route: ActivatedRoute,private http: HttpClient) {
    
  }

  ngOnInit() {
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', 'Token '+ this.auth.token);
    this.http.get("http://localhost:8000/vendor-service/v1/distributors/" + this.id + "/shopkeepers/",{headers: headers}).subscribe((data: Retailer[]) => {
      this.retailers = data
    }), error => {
      alert("Login Error");
      console.error(error);
    };
}
}
