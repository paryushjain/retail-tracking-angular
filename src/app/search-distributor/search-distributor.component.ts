import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Distributor} from '../models/distributor';

@Component({
  selector: 'app-search-distributor',
  templateUrl: './search-distributor.component.html',
  styleUrls: ['./search-distributor.component.css'],
})
export class SearchDistributorComponent implements OnInit {

distributors:Distributor[];
auth: any = JSON.parse(localStorage.getItem('auth'));

  constructor(private http:HttpClient) { }

  ngOnInit() {
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', 'Token '+ this.auth.token);

    this.http.get("http://localhost:8000/vendor-service/v1/distributors/",{headers: headers}).subscribe((data:Distributor[])=>{
          this.distributors=data
    }),error=>{
      alert("Login Error");
      console.error(error);
    };
  }

}
