import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;
  loginType: string;

  constructor(private authService: AuthService, private router: Router) { }


  ngOnInit() {
    if (this.authService.isAuthenticated()) this.router.navigate(["dashboard"]);
  }

  login() {
    if(this.loginType=="distributor"){
    this.authService.login(this.username, this.password, this.loginType).subscribe((data: any) => {
      let expiry = new Date();
      expiry.setTime(expiry.getTime() + 20 * 60 * 1000);
      localStorage.setItem("auth", JSON.stringify({ token: data.token, expiry: expiry, id: data.distributor_id, loginType: this.loginType }))
      this.router.navigate(["dashboard/myProfile"]);
    }), error => {
      alert("Login Error");
      console.error(error);
    };
  }else if(this.loginType=="retailer"){
    this.authService.login(this.username, this.password, this.loginType).subscribe((data: any) => {
      let expiry = new Date();
      expiry.setTime(expiry.getTime() + 20 * 60 * 1000);
      localStorage.setItem("auth", JSON.stringify({ token: data.token, expiry: expiry, id: data.shopkeeper_id, loginType: this.loginType }))
      this.router.navigate(["dashboard"]);
    }), error => {
      alert("Login Error");
      console.error(error);
    };
  }

  }

}
