import { Component, OnInit } from '@angular/core';
import { Retailer } from '../models/retailer';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-search-retailer',
  templateUrl: './search-retailer.component.html',
  styleUrls: ['./search-retailer.component.css']
})
export class SearchRetailerComponent implements OnInit {

  retailers: Retailer[];
  auth: any = JSON.parse(localStorage.getItem('auth'));
  id: string = this.auth.id;

  constructor(private http:HttpClient) { }

  ngOnInit() {
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', 'Token '+ this.auth.token);

    this.http.get("http://localhost:8000/vendor-service/v1/distributors/" + this.id + "/shopkeepers/",{headers: headers}).subscribe((data:Retailer[])=>{
          this.retailers=data
    }),error=>{
      alert("Login Error");
      console.error(error);
    };
  }

}
