import { Pipe, PipeTransform } from '@angular/core';
import { Distributor } from './models/distributor';

@Pipe({
    name: 'LockFilter'
})

export class FilterPipe implements PipeTransform {
    transform(value: any, args?: any): any {

        if(!value)return null;
        if(!args)return value;

        args = args.toLowerCase();

        return value.filter(function(item:Distributor){
            return item.address.toLowerCase().includes(args) || item.registered_name.toLowerCase().includes(args);
        });
    }
}