import { Injectable } from '@angular/core';
import { Distributor } from '../models/distributor';
import { Retailer } from '../models/retailer';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  auth: any = JSON.parse(localStorage.getItem('auth'));
  distributor: Distributor;
  retailer: Retailer;
  

  constructor(private http: HttpClient) {
    
   }

saveUserData(){
  let headers = new HttpHeaders();
    headers = headers.set('Authorization', 'Token '+ this.auth.token);
  if (this.auth.loginType=="distributor") {
    this.http.get("http://localhost:8000/vendor-service/v1/distributors/" + this.auth.id,{headers:headers}).subscribe((data: Distributor) => {
      this.distributor = data
    }), error => {
      alert("Login Error");
      console.error(error);
    };
  } else if (this.auth.loginType=="retailer") {
    this.http.get("http://localhost:8000/vendor-service/v1/shopkeepers/" + this.auth.id,{headers:headers}).subscribe((data: Retailer) => {
      this.retailer = data
    }), error => {
      alert("Login Error");
      console.error(error);
    };
  }
}

getDistributor(){
  return this.distributor;
}
getRetailer(){
  return this.retailer;
}


}
