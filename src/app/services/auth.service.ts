import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  login(username, password, loginType): Observable<any> {
    if (loginType == "distributor") {
      return this.http.post<Observable<any>>("http://localhost:8000/vendor-service/v1/distributors/login/", { username: username, password: password });
    } else if (loginType == "retailer") {
      return this.http.post<Observable<any>>("http://localhost:8000/vendor-service/v1/shopkeepers/login/", { username: username, password: password });
    }
  }

  distributorSignup(username, password, email, established_year, registered_name, contact_number, aadhar, pan, gstn, address){
    return this.http.post("http://localhost:8000/vendor-service/v1/distributors/signup/", {
      user: { username: username, password: password, email: email },
      established_year: established_year,
      registered_name: registered_name,
      contact_number: contact_number,
      aadhar: aadhar,
      pan: pan,
      gstn: gstn,
      address: address
    }).subscribe((response)=>{
      console.log('repsonse ',response);
    });
  }

  retailerSignup(username, password, email, established_year, registered_name, contact_number, aadhar, pan, gstn, address){
    return this.http.post("http://localhost:8000/vendor-service/v1/shopkeepers/signup/", {
      user: { username: username, password: password, email: email },
      established_year: established_year,
      registered_name: registered_name,
      contact_number: contact_number,
      aadhar: aadhar,
      pan: pan,
      gstn: gstn,
      address: address
    }).subscribe((response)=>{
      console.log('repsonse ',response);
    });
  }


  isAuthenticated(): boolean {
    let auth: any = JSON.parse(localStorage.getItem('auth'));
    let currentTime = new Date().getTime();
    let expiryTime = new Date(auth.expiry).getTime();
    return auth.token != null && expiryTime >= currentTime;

  }


}
