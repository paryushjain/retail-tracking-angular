import { Component, OnInit, Input, Injectable } from '@angular/core';
import { Distributor } from '../models/distributor';
import { HttpClient } from '@angular/common/http';
import { DataService } from '../services/data.service';
import { Retailer } from '../models/retailer';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})

export class ProfileComponent implements OnInit {

  auth: any = JSON.parse(localStorage.getItem('auth'));
  loginType:any = this.auth.loginType;
  distributor: Distributor;
  retailer: Retailer;
  distributor_t:string = "distributor";
  retailer_t:string = "retailer";


  constructor(private data: DataService) { }

  ngOnInit() {
    this.data.saveUserData();
    if(this.loginType=="distributor"){
      this.distributor = this.data.getDistributor();
    }else if(this.loginType=="retailer") {
      this.retailer = this.data.getRetailer();
    }
  }


}
