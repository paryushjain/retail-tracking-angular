import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { HeaderComponent } from './header/header.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { SearchRetailerComponent } from './search-retailer/search-retailer.component';
import { SearchDistributorComponent } from './search-distributor/search-distributor.component';
import {FilterPipe} from './filter.pipe';
import { ProfileComponent } from './profile/profile.component';
import { ViewCustomerComponent } from './view-customer/view-customer.component';
import { DataService } from './services/data.service';
import { ViewProfileComponent } from './view-profile/view-profile.component';
import { ViewSearchRetailerProfileComponent } from './view-search-retailer-profile/view-search-retailer-profile.component';
import { ViewSearchDistributorProfileComponent } from './view-search-distributor-profile/view-search-distributor-profile.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    SignupComponent,
    HeaderComponent,
    DashboardComponent,
    SearchRetailerComponent,
    SearchDistributorComponent,
    FilterPipe,
    ProfileComponent,
    ViewCustomerComponent,
    ViewProfileComponent,
    ViewSearchRetailerProfileComponent,
    ViewSearchDistributorProfileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
    
  ],
  providers: [HttpClient, DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
