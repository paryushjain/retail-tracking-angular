export class Distributor {
    distributor_id: string;
    registered_name: string;
    contact_number: string;
    aadhar: string;
    pan: string;
    gstn: string;
    address: string;
    established_year: string
}