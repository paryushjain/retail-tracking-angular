import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Retailer } from '../models/retailer';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-view-profile',
  templateUrl: './view-profile.component.html',
  styleUrls: ['./view-profile.component.css']
})
export class ViewProfileComponent implements OnInit {

  shopkeeper_id: string;
  crI: Number;
  crA: Number;
  duration: Number;
  auth: any = JSON.parse(localStorage.getItem('auth'));
  distributor_id: string = this.auth.id;
  retailer: Retailer;

  constructor(private route: ActivatedRoute, private http: HttpClient, private router:Router) { }

  ngOnInit() {
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', 'Token '+ this.auth.token);
    this.shopkeeper_id = this.route.snapshot.paramMap.get("id");
    this.crI = +this.route.snapshot.paramMap.get("id1");
    this.crA = +this.route.snapshot.paramMap.get("id2");
    this.http.get("http://localhost:8000/vendor-service/v1/shopkeepers/" + this.shopkeeper_id,{headers: headers}).subscribe((data: Retailer) => {
      this.retailer = data
    }), error => {
      alert("Login Error");
      console.error(error);
    };
  }

  updateCreditRating() {
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', 'Token '+ this.auth.token);
    return this.http.put("http://localhost:8000/vendor-service/v1/enrollments/",
      {
        distributor_id: +this.distributor_id,
        shopkeeper_id: +this.shopkeeper_id,
        payment_cycle_duration: +this.duration
      },{headers: headers}).
      subscribe(
        data => {
          console.log("PUT Request is successful ", data);
          this.ngOnInit();
        },
        error => {
          console.log("Error", error);
        }
      );
  }

}
