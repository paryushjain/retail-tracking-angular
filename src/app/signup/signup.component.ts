import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  registered_name: string;
  contact_number: string;
  aadhar: string;
  pan: string;
  gstn: string;
  address: string;
  established_year: string
  username: string;
  password: string;
  email: string;
  signupType: string;

  constructor(private authService: AuthService,private router:Router) { }

  ngOnInit() {
  }

  signup(){
    if(this.signupType == "distributor"){
    this.authService.distributorSignup(this.username, this.password, this.email, this.established_year, this.registered_name, this.contact_number, this.aadhar, this.pan, this.gstn, this.address);
    }else if(this.signupType == "retailer"){
      this.authService.retailerSignup(this.username, this.password, this.email, this.established_year, this.registered_name, this.contact_number, this.aadhar, this.pan, this.gstn, this.address);
    }
    this.router.navigate(['/']);
  }

}
