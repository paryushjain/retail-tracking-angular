import { Component, OnInit } from '@angular/core';
import { Distributor } from '../models/distributor';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-view-search-distributor-profile',
  templateUrl: './view-search-distributor-profile.component.html',
  styleUrls: ['./view-search-distributor-profile.component.css']
})
export class ViewSearchDistributorProfileComponent implements OnInit {

  distributor_id: string;
  auth: any = JSON.parse(localStorage.getItem('auth'));
  distributor: Distributor;

  constructor(private route: ActivatedRoute, private http: HttpClient, private router:Router) { }

  ngOnInit() {
    let headers = new HttpHeaders();
    headers = headers.set('Authorization', 'Token '+ this.auth.token);
    this.distributor_id = this.route.snapshot.paramMap.get("id");
    this.http.get("http://localhost:8000/vendor-service/v1/distributors/" + this.distributor_id,{headers: headers}).subscribe((data: Distributor) => {
      this.distributor = data
    }), error => {
      alert("Login Error");
      console.error(error);
    };
  }

}
